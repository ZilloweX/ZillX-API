<?php
// Read the JSON file
$data = file_get_contents('json/data.json');

// Convert JSON to PHP array
$items = json_decode($data, true);

if (isset($_GET['id'])) {
    $itemId = $_GET['id'];
    foreach ($items['data'] as $item) {
        if ($item['id'] == $itemId) {
            header('Content-Type: application/json');
            echo json_encode($item);
            exit;
        }
    }
}

// If the item is not found, return a 404 response
http_response_code(404);
echo json_encode(array("message" => "Item not found"));
?>
