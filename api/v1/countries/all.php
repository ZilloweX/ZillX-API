<?php
if (isset($_GET['all'])) {
    // Get the query parameters
    $all = $_GET['all'];

    // Construct the external API URL
    $apiUrl = "https://restcountries.com/v3.1/all";

    // Fetch data from the external API using file_get_contents
    $response = file_get_contents($apiUrl);

    // Check if the request was successful
    if ($response === false) {
        http_response_code(500);
        echo json_encode(array("message" => "Failed to retrieve data from the external API."));
        exit;
    }

    // Parse the JSON response
    $data = json_decode($response);

    // Check if the JSON decoding was successful
    if ($data === null) {
        http_response_code(500);
        echo json_encode(array("message" => "Failed to parse JSON response from the external API."));
        exit;
    }

    // Return the data from the external API as a response
    header('Content-Type: application/json');
    echo json_encode($data);
} else {
    // If the required query parameters are not provided, return a 400 response
    http_response_code(400);
    echo json_encode(array("message" => "Missing required query parameters (all)."));
}

?>
