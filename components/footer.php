<div class="footer">
  <a href="https://codeberg.org/ZilloweX/ZillX-API"><img src='/public/img/codeberg.svg'/></a>
  <p>&copy; <?php echo date('Y'); ?> Zillowe Foundation | ZilloweX | ZillX API 0.1 | All rights reserved.</p>
</div>