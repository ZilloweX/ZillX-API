<head>
  <title>Zillowe | ZilloweX | ZillX API 0.1</title>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="/public/img/Xlogo.png" type="image/x-icon">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <style>
    /* Dark Theme Styles */
    * {
      transition: 0.3s;
    }
    *:hover {
      transition: 0.3s;
    }
    body {
      background-color: #1e1e1e;
      color: #fff;
    }
    .container {
      padding-top: 20px;
      padding-bottom: 20px;
    }
    .panel {
      background-color: #333;
      border: 1px solid #444;
      border-radius: 5px;
    }
    .panel-heading {
      background-color: #222;
      border-bottom: 1px solid #444;
      border-radius: 5px 5px 0 0;
      color: #fff;
    }
    .panel-body {
      background-color: #333;
      border-radius: 0 0 5px 5px;
    }
    .form-group {
      margin-right: 10px;
      font-size: 2rem!important;
    }
    .btn-primary {
      background-color: #007bff;
      border-color: #007bff;
    }
    .btn-primary:hover {
      background-color: #0056b3;
      border-color: #0056b3;
    }
    .footer {
      text-align: center;
      padding: 10px 0;
      width: 100%;
      text-align: center;
      line-height: 60px;
      position: absolute;
    }
  </style>
</head>