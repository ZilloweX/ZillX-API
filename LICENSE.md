# ZilloweX Public License
#### 2023-9-27, Version 1.0, by Zillowe Foundation

## Definitions
### ZXPL
ZilloweX Public License.

### ZilloweX
A part of the Zillowe Foundation.

### We or Us or Our or I or me or mine
We as a company,
I as an Individual.

### You or Your
The end-user or the company who is accessing this product.

### Company
A Corporation or an Organization or a Foundation.

### Product
This App or this Software or a Program or a part of App or a part of Software or a part of Program, or even Images or whatever inside this product.

### Brand
The text or the sound or the images or the files that represent this company.

### Bug
A problem that can cause the product to crash or stop working completely or stop working some of it functionally, it caused by the product.

## The Rules
### 1
You can copy this product and everything in it without asking.

But you should not copy the brand and pretend it's yours.

### 2
If you found a bug in this product, you can fork it and make a version without that bug and share it freely.

But if you want to share it, it should be under this license.

### 3
Any product based of a product under this license, it should be under this license as well.

### 4
This license is for free and open source products, this product can't be paid or closed source, the source code should be always available to the public and not behind a paywall or needs and account to access it or fork it locally.

### 5
This license cannot be changed by anyone, just the Zillowe Foundation and it parts can change it.

### 6
Any updates to the license are applied automatically.

### 7
This product shouldn't be have any kind trackers
## Footer
This  is the ZXPL version 1.0, by Zillowe Foundation and ZilloweX.

To the updates visit https://zilx.rf.gd/zxpl