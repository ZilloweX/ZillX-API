<!DOCTYPE html>
<html lang="en">
<?php include 'components/head.php' ?>
<body>
  

<div class="container">
  <div class="row">
    <!-- Side Panel -->
    <div class="col-md-3">
      <div class="panel">
        <div class="panel-heading"><img src='/public/img/Xlogo.png' width='40px' height='40px'/>ZilloweX</div>
        <div class="panel-body">
          <span>To see the data IDs visit | Full Doc</span>
          <a href="https://docs.zillowex.rf.gd/zillxapi" class="btn btn-primary btn-block">Documentation</a>
          <br/>
          <span>Quick Doc</span>
          <a href="/doc" class="btn btn-primary btn-block">Doc</a>
          <br/>
          <span>To see the code visit</span>
          <a href="https://codeberg.org/ZilloweX/ZillX-API" class="btn btn-primary btn-block">Codeberg</a>
        </div>
      </div>
    </div>

    <!-- Main Content -->
    <div class="col-md-9">
      <h2>ZillX API 0.1</h2>
      <form class="form-inline" action="" method="POST">
        <div class="form-group">
          <label for="id">Enter Data ID</label>
          <input type="text" name="id" class="form-control"  placeholder="Enter Data ID" required/>
        </div>
        <button type="submit" name="submit" class="btn btn-primary">Get Data</button>
      </form>
      <div class="search-results">
        <h3>Search Results:</h3>
        <?php
          if(isset($_POST['submit']))
          {
              $id = $_POST['id'];
              
              // Read the JSON file
              $data = file_get_contents('api/v1/json/data.json');

              // Convert JSON to PHP array
              $json_data = json_decode($data, true);

              // Find the data by ID
              $dataItem = null;
              foreach ($json_data['data'] as $item) {
                  if ($item['id'] == $id) {
                      $dataItem = $item;
                      break;
                  }
              }

              function displayData($data) {
                echo "<ul>";
                foreach ($data as $key => $value) {
                    echo "<li><strong>$key:</strong>";
                    if (is_array($value)) {
                        // If the value is an array, recursively call the function
                        displayData($value);
                    } else {
                        // If it's not an array, display the value directly
                        echo "$value";
                    }
                    echo "</li>";
                }
                echo "</ul>";
            }
            
            if ($dataItem !== null) {
                echo "<h4>Data Details:</h4>";
                displayData($dataItem);
            } else {
                echo "<p>No data found with that ID.</p>";
            }
            
          }
        ?>
      </div>
    </div>
  </div>
</div>

<?php include 'components/footer.php' ?>

</body>
</html>
