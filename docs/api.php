<!DOCTYPE html>
<html lang="en">
<?php include '../components/head.php' ?>
<body>
  

<div class="container">
  <div class="row">
    <!-- Side Panel -->
    <?php include '../components/doc_panel.php' ?>

    <!-- Main Content -->
    <div class="col-md-9">
      <h2>ZillX API 0.1 Doc</h2>
      <div class="form-inline">
        <div class="form-group">
          API
        </div>
      </div>
      <hr/>
      <div class="search-results">
        The first end point is <a href="/api/v1/api">https://zxapi.zillowex.rf.gd/api/v1/api</a><br/>
        put '?' in the end of the url and add 'id=' then put the id number that you want.<br/>
        <br/>
        The second end point is <a href="/api/v1/api-crypto">https://zxapi.zillowex.rf.gd/api/v1/crypto</a><br/>
        put '?' in the end of the url and add 'coin1=' then put the coin that you want<br/>
        then put '&coin2=' and put the coin that you want to convert to<br/>
        you can more than one coin in 'coin2=' by adding ',' between every coin.<br/>
        <br/>
        The third end point is <a href="/api/v1/countries/name">https://zxapi.zillowex.rf.gd/api/v1/countries/name</a><br/>
        put '?' in the end of the url and add 'name=' then put the name of the country that you want to get information from<br/>
        <br/>
        The fourth end point is <a href="/api/v1/countries/code">https://zxapi.zillowex.rf.gd/api/v1/countries/code</a><br/>
        put '?' in the end of the url and add 'code=' then put the code of the country that you want to get information from<br/>
        <br/> 
        same thing for those<br/>
        https://zxapi.zillowex.rf.gd/api/v1/countries/capital<br/>
        https://zxapi.zillowex.rf.gd/api/v1/countries/lang<br/>
        https://zxapi.zillowex.rf.gd/api/v1/countries/subregion<br/>
        https://zxapi.zillowex.rf.gd/api/v1/countries/region<br/>
        https://zxapi.zillowex.rf.gd/api/v1/countries/currency<br/>
        <br/>
        to see list of all the countries<br/>
        https://zxapi.zillowex.rf.gd/api/v1/countries/all?all=all<br/>
      </div>
    </div>
  </div>
</div>

<?php include '../components/footer.php' ?>

</body>
</html>
