<!DOCTYPE html>
<html lang="en">
<?php include '../components/head.php' ?>
<body>
  

<div class="container">
  <div class="row">
    <!-- Side Panel -->
    <?php include '../components/doc_panel.php' ?>

    <!-- Main Content -->
    <div class="col-md-9">
      <h2>ZillX API 0.1 Doc</h2>
      <div class="form-inline">
        <div class="form-group">
          Example
        </div>
      </div>
      <hr/>
      <div class="search-results">
        <h4>1: Data</h3>
        <a href="/api/v1/api?id=1">https://zxapi.zillowex.rf.gd/api/v1/api?id=1</a><br/>
        <br/>
        <h4>2: Crypto</h3>
        <a href="/api/v1/crypto?coin1=BTC&coin2=USD">https://zxapi.zillowex.rf.gd/api/v1/crypto?coin1=BTC&coin2=USD</a><br/>
        <br/>
        <h4>3: Countries</h3>
        <a href="/api/v1/countries/name?name=United States of America">https://zxapi.zillowex.rf.gd/api/v1/countries/name?name=United States of America</a><br/>
        <br/>
        <h4>3: Code</h3>
        <a href="/api/v1/countries/code?code=USD">https://zxapi.zillowex.rf.gd/api/v1/countries/code?code=USA</a><br/>
        <br/>
        <h4>4: Capital</h3>
        <a href="/api/v1/countries/capital?capital=Washington, D.C.">https://zxapi.zillowex.rf.gd/api/v1/countries/capital?capital=Washington, D.C.</a><br/>
        <br/>
        <h4>5: Language</h3>
        <a href="/api/v1/countries/lang?lang=english">https://zxapi.zillowex.rf.gd/api/v1/countries/lang?lang=english</a><br/>
        <br/>
        <h4>6: Region</h3>
        <a href="/api/v1/countries/region?region=Americas">https://zxapi.zillowex.rf.gd/api/v1/countries/region?region=Americas</a><br/>
        <br/>
        <h4>6: Subregion</h3>
        <a href="/api/v1/countries/subregion?subregion=North America">https://zxapi.zillowex.rf.gd/api/v1/countries/subregion?subregion=North America</a><br/>
        <h4>7: All</h3>
        <a href="/api/v1/countries/all?all=all">https://zxapi.zillowex.rf.gd/api/v1/countries/all?all=all</a><br/>
      </div>
    </div>
  </div>
</div>

<?php include '../components/footer.php' ?>

</body>
</html>
